'use strict';

const _ = require('lodash');
const fs = require('fs');
const moment = require('moment');
const uuid = require('node-uuid');
const mime = require('mime');
const yaml = require('js-yaml');
const AWS = require('aws-sdk');
const co = require('co');
const JSZip = require('jszip');
const request = require('request');
const corequest = require('co-request');
const Validator = require('jsonschema').Validator;
const dbc = require('dynamodb-marshaler');

const zipFilePrefix = 'zip-tmp/';
const archiveFilePrefix = 'archive/';

const zipSchema = yaml.safeLoad(fs.readFileSync('./zipSchema.yaml', 'utf8'));
AWS.config.loadFromPath('./auth-config.json');
const s3 = new AWS.S3({apiVersion: '2006-03-01', region: 'us-west-2'});
const dynamoDb = new AWS.DynamoDB({apiVersion: '2012-08-10', region: 'us-west-2'});
const lambda = new AWS.Lambda({apiVersion: '2015-03-31', region: 'us-west-2'});

const v = new Validator();

function checkSchema(event) {
    //Its cheking the schema based on the zipSchema.yaml file
    const result = v.validate(event, zipSchema);
    if (!result.valid) {
        throw Error(result.errors);
    }
    return event;
}

function *checkTarget(event) {
    if (event.target == null) {
        event.target = {};
    }
    if (_.get(event.target, 'bucket', null) == null) {
        event.target.bucket = event.source.bucket;
    }
    if (event.target.key) {
        const params = {
            Bucket: event.target.bucket,
            Key: event.target.key
        };
        try {
            const target = yield s3.headObject(params).promise();
        } catch (error) {
            if (error.statusCode != 404) {
                throw error;
            }
        }
    } else {
        event.target.key = `${zipFilePrefix}${uuid.v4()}.zip`;
    }
    return event;
}

const s3zipTmpRule = {
    Prefix: zipFilePrefix,
    Status: 'Enabled',
    Expiration: {
        Days: 1
    },
    ID: 's3zip-tmp-'
};

function *createLifecycle(event) {
    const newRule = _.cloneDeep(s3zipTmpRule);
    newRule.ID = newRule.ID + uuid.v4();
    let params = {
        Bucket: event.target.bucket,
        LifecycleConfiguration: {
            Rules: [s3zipTmpRule]
        }
    };
    return yield s3.putBucketLifecycleConfiguration(params).promise();
}

function *addLifecycleRule(lifecycleConfiguration, event) {
    const newLifecycleConfiguration = _.cloneDeep(lifecycleConfiguration);
    const newRule = _.cloneDeep(s3zipTmpRule);
    newRule.ID = newRule.ID + uuid.v4();
    newLifecycleConfiguration.Rules.push(newRule);
    let params = {
        Bucket: event.target.bucket,
        LifecycleConfiguration: newLifecycleConfiguration
    };
    return yield s3.putBucketLifecycleConfiguration(params).promise();
}

function *checkLifecycle(event) {
    let params = {
        Bucket: event.target.bucket
    };
    let result = null;
    try {
        result = yield s3.getBucketLifecycleConfiguration(params).promise();
    } catch (err) {
        if (err.code != 'NoSuchLifecycleConfiguration') {
            throw err;
        }
    }
    if (result == null) {
        result = yield createLifecycle(event);
    } else {
        if (!_.find(result.Rules, {Prefix: s3zipTmpRule.Prefix})) {
            result = yield addLifecycleRule(result, event);
        }
    }
}

function *checkAccess(bucket, key, newName) {
    let result = null;
    result = yield corequest({
        url: `https://s3-us-west-2.amazonaws.com/${bucket}/${key}`,
        method: 'HEAD'
    });
    return {
        bucket: bucket,
        id: key,
        fileName: newName,
        size: parseInt(result.headers['content-length'], 10),
        statusCode: _.get(result, 'statusCode'),
        statusMessage: _.get(result, 'statusMessage')
    };
}

function transform(event) {
    const params = {
        source: {}
    };
    params.source.bucket = _.get(event, 'pathParameters.BucketId');
    var local = _.get(event, 'body', '');
    if (local != '') {
        params.source.keys = JSON.parse(local).data;
    }
    return params;
}

function *processInput(event) {

    event = checkSchema(transform(event));
    event = yield checkTarget(event);
    yield checkLifecycle(event);
    event.sizes = yield event.source.keys.map(key => checkAccess(event.source.bucket, key.id, key.fileName));
    event = yield handleErrors(event);
    return event;
}

function *handleErrors(event) {
    if (event.source.error == null) {
        event.source.error = [];
    }
    var local = _.remove(event.sizes, function (o) {
        return o.statusCode != 200;
    });
    event.source.error = local;
    _.forEach(local, function (o) {
        delete o.size;
        delete o.bucket;
        _.remove(event.source.keys, {'id': o.id})
    });
    return event;
}

function *addZipDbEntry(zipId, keys, bucket, key, sizeContents) {
    var keyIds = [];
    for (var i in keys) {
        keyIds.push(keys[i].id);
    }
    var uniqKeys = _.uniq(keyIds);
    if (uniqKeys.length > 0) {
        const params = {
            Item: {
                zipId: {
                    S: zipId
                },
                keys: {
                    SS: uniqKeys
                },
                bucket: {
                    S: bucket
                },
                key: {
                    S: key
                },
                timeStart: {
                    S: moment().toISOString()
                },
                status: {
                    S: 'inProgress'
                },
                sizeContents: {
                    N: sizeContents.toString()
                }
            },
            TableName: 's3zip'
        };
        let result = yield dynamoDb.putItem(params).promise();
        return dbc.toJS(result);
    } else {
        throw ({
            statusCode: 404,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify({'error': 'No valid files'})
        });
    }
}

function *completedZipDbEntry(zipId, url) {
    let result = yield corequest({url: url, method: 'HEAD'});
    if (_.get(result, 'statusCode') != 200) {
        throw( {name: 'ObjectAccess', url: url, error: {statusCode: result.statusCode}} );
    }
    const sizeZip = parseInt(result.headers['content-length'], 10);

    const params = {
        Key: {
            zipId: {
                S: zipId
            }
        },
        AttributeUpdates: {
            status: {
                Value: {
                    S: 'complete'
                }
            },
            timeEnd: {
                Value: {
                    S: moment().toISOString()
                }
            },
            sizeZip: {
                Value: {
                    N: sizeZip.toString()
                }
            },
            url: {
                Value: {
                    S: url
                }
            }
        },
        TableName: 's3zip'
    };

    return yield dynamoDb.updateItem(params).promise();
}

function *saveDbError(zipId, message) {
    const params = {
        Key: {
            zipId: {
                S: zipId
            }
        },
        AttributeUpdates: {
            status: {
                Value: {
                    S: 'failed'
                }
            },
            message: {
                Value: {
                    S: message
                }
            }
        },
        TableName: 's3zip'
    };
    return yield dynamoDb.updateItem(params).promise();
}

function *processFiles(dbRecord, body, getRemainingTimeInMillis) {
    const result = new Promise(function (resolve, reject) {
        const zip = new JSZip();
        // This adds a Node.js ReadableStream for each file to the JSZip object
        body.success.map(key => {
            zip.file(key.fileName, request(`https://s3-us-west-2.amazonaws.com/${dbRecord.bucket}/${key.id}`));
        });

        // This sets the target S3 bucket location and create the Nodejs stream that will produce all of the joined zip data
        const s3Params = {
            Bucket: dbRecord.bucket,
            Key: `${zipFilePrefix}${dbRecord.zipId}.zip`,
            Body: zip.generateNodeStream({streamFiles: true}),
            ACL: 'public-read'
        };

        // This sets a maximum of 20 5MB parts which means a total of 100MB of concurrent data.
        const options = {partSize: 5 * 1024 * 1024, queueSize: 20};
        const upload = s3.upload(s3Params, options);

        // This sets an event handler will output the current progress of the uplaod and check how much time is left to run.
        upload.on('httpUploadProgress', function (progress) {
            if (getRemainingTimeInMillis() < 5000) {
                reject({
                    error: 'Timeout',
                    message: `Ran out of processing time. To avoid this error either increase the amount of time that can be used to execute, choose a smaller number of files, smaller file size, or contact the application owner`
                });
            }
        });
        upload.send(function (err, data) {
            console.log("process files: ", body);
            return err ? reject({error: err}) : resolve(data);
        });
    });

    return result;
}

function *listBuckets() {
    const result = yield s3.listBuckets().promise();
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(result.Buckets.map(item => item.Name))
    };
}

function *listKeys(BucketId) {
    const result = yield s3.listObjectsV2({Bucket: BucketId}).promise();
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(result.Contents.map(item => item.Key))
    };
}

function *moveKeys(event) {
    var body = JSON.parse(_.get(event, "body", ""));
    let result = yield body.data.map(key => copyKeys(key.id, event.pathParameters.BucketId));
    result = yield deleteKeys(_.compact(_.map(result, 'id')), event.pathParameters.BucketId);
    return result;
}
function *copyKeys(key, bucketId) {
    let result = '';
    const params = {
        Bucket: bucketId,
        CopySource: `${bucketId}/${key}`,
        Key: `${archiveFilePrefix}${key}`
    };
    console.log(params);
    try {
        result = yield s3.copyObject(params).promise();
        result.id = key;
    } catch (error) {
        return {};
    }
    console.log(result);
    return result;
}
function *deleteKeys(keys, bucketId) {
    const params = {
        Bucket: bucketId,
        Delete: {
            Objects: keys.map(function (item) {
                return {Key: item};
            })
        }
    };
    console.log(params);
    const result = yield s3.deleteObjects(params).promise();
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(result)
    };
}
function *createZipId(event, context) {
    let result = null;
    const params = yield processInput(event);
    const zipId = uuid.v4();
    let body = {
        zipId: zipId,
        zipURL: `https://s3-us-west-2.amazonaws.com/${params.target.bucket}/${zipFilePrefix}${zipId}.zip`,
        success: params.source.keys,
        error: params.source.error
    };
    let headers = {
        'Access-Control-Allow-Origin': '*'
    };

    const totalSize = _.reduce(params.sizes, (accum, item) => {
        accum += item.size;
        return accum;
    }, 0);
    yield addZipDbEntry(zipId, params.source.keys, params.target.bucket, params.target.key, totalSize);
    console.log("CreateZipId body: ", body);
    return body;
}

function *getDbRecord(zipId) {
    const params = {
        Key: {
            zipId: {
                S: zipId
            }
        },
        TableName: 's3zip'
    };
    let result = yield dynamoDb.getItem(params).promise();
    result = dbc.toJS(result.Item);
    return result;
}

function *processZip(event, context) {
    var body = JSON.parse(event.body);
    let dbRecord = yield getDbRecord(body.zipId);
    let result = yield processFiles(dbRecord, body, context.getRemainingTimeInMillis);
    yield completedZipDbEntry(dbRecord.zipId, result.Location, result.sizeContents);
    return result;
}

function getSignedUrlPromise(params) {
    return new Promise(function (resolve, reject) {
        s3.getSignedUrl('putObject', params, function (err, url) {
            if (err) return reject(err);
            return resolve(url);
        });
    });
}

function *createUploadUrl(key, BucketId) {
    const params = {
        Bucket: BucketId,
        Key: key,
        ContentType: mime.lookup(key),
        ACL: 'public-read'
    };
    const url = yield getSignedUrlPromise(params);
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({url: url, mime: mime.lookup(key)})
    };
}

function *getZipId(zipId) {
    const params = {
        Key: {
            zipId: {
                S: zipId
            }
        },
        TableName: 's3zip'
    };
    const results = yield dynamoDb.getItem(params).promise();
    var res = (dbc.toJS(results.Item));
    delete res.keys;
    if (res.timeEnd && res.timeStart) {
        res.timeDiff = moment(res.timeEnd).diff(res.timeStart);
    }
    return {
        statusCode: 200,
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify(res)
    };
}

function *invokeEvent(event) {
    const params = {
        FunctionName: 's3zip',
        InvocationType: 'Event',
        LogType: 'None',
        Payload: JSON.stringify(event)
    };
    return yield lambda.invoke(params).promise();
}

exports.handler = function (event, context, callback) {
    console.log('ENTRY : ', event);
    co(function*() {
        context.callbackWaitsForEmptyEventLoop = false;
        let result = null;
        if (event.resource == '/buckets' && event.httpMethod == 'GET') {
            result = yield listBuckets();
        } else if (event.resource == '/buckets/{BucketId}' && event.httpMethod == 'GET') {
            result = yield listKeys(event.pathParameters.BucketId);
        } else if (event.resource == '/buckets/{BucketId}' && event.httpMethod == 'DELETE') {
            event.httpMethod = 'PATCH';
            yield invokeEvent(event);
            console.log("is it getting here?");
            result = {
                statusCode: 200,
                headers: {'Access-Control-Allow-Origin': '*'},
                body: JSON.stringify({'status': 'Request Received.'})
            };
        } else if (event.resource == '/buckets/{BucketId}' && event.httpMethod == 'PATCH') {
            console.log("Patch");
            result = yield moveKeys(event);
        } else if (event.resource == '/buckets/{BucketId}/upload-url' && event.httpMethod == 'POST') {
            result = yield createUploadUrl(event.queryStringParameters.key, event.pathParameters.BucketId);
        } else if (event.resource == '/buckets/{BucketId}/zip/{ZipId}' && event.httpMethod == 'GET') {
            console.log("its getting here");
            result = yield getZipId(event.pathParameters.ZipId);
        } else if (event.resource == '/buckets/{BucketId}/zip' && event.httpMethod == 'PUT') {
            try {
                result = yield processZip(event, context);
            } catch (error) {
                yield saveDbError(JSON.parse(event.body).zipId, error.message);
            }
        } else if (event.resource == '/buckets/{BucketId}/zip' && event.httpMethod == 'POST') {
            var res = yield createZipId(event, context);
            event.httpMethod = 'PUT';
            event.body = JSON.stringify(res);
            delete res.success;
            result = {
                statusCode: 201,
                headers: {'Access-Control-Allow-Origin': '*'},
                body: JSON.stringify(res)
            };
            yield invokeEvent(event);
        }

        return result;
    }).then(function (value) {
        console.log("RESPONSE: ", value);
        callback(null, value);
    }).catch(function (error) {
        console.log("ERROR: ", error);
        var response = error.statusCode ? error : {
            statusCode: 400,
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(error, null, 2)
        };
        callback(null, response);
    });
};

// This is purely for testing on a local developer system. It is never invoked within Lambda
if (!module.parent) {
    const sampleEvent = {
        resource: "/buckets/{BucketId}/zip",
        path: "/s3zip/zip/f13fceed-2d7c-44b0-9076-8d8fd23e1c30",
        httpMethod: "POST",
        headers: null,
        queryStringParameters: {},
        pathParameters: {
            BucketId: "s3zip",
            ZipId: "f13fceed-2d7c-44b0-9076-8d8fd23e1c30"
        },
        stageVariables: null,
        requestContext: {
            accountId: "035505386529",
            resourceId: "ff9qwj",
            stage: "test-invoke-stage",
            requestId: "test-invoke-request",
            identity: {
                cognitoIdentityPoolId: null,
                accountId: "035505386529",
                cognitoIdentityId: null,
                caller: "035505386529",
                apiKey: "test-invoke-api-key",
                sourceIp: "test-invoke-source-ip",
                cognitoAuthenticationType: null,
                cognitoAuthenticationProvider: null,
                userArn: "arn:aws:iam::035505386529:root",
                userAgent: "Apache-HttpClient/4.5.x (Java/1.8.0_102)",
                user: "035505386529"
            },
            resourcePath: "/buckets/{BucketId}/zip/{ZipId}",
            apiId: "0zx5r0w1ni"
        },
        body: '{"data":[{"id":"file1.pdf","fileName":"test123.PDF","outletNumber":"2323","category":"sdfsdf","chainName":"dsfsfs"},{"id":"file2.pdf ","fileName":"test234.pdf","outletNumber":"2323","category":"sdfsdf","chainName":"dsfsfs"}]}'


    };
    exports.handler(sampleEvent, {
        getRemainingTimeInMillis: function () {
            return 12345;
        }
    }, function (error, value) {
        if (error) {
            console.error("error node", error);
            process.exit(1);
        } else {
            console.log("success node", value);
            process.exit(0);
        }
    });
}
