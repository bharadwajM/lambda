'use strict';

const AWS = require('aws-sdk');
const co = require('co');
const spawn = require('co-child-process');
const fs = require('fs');
const nconf = require('nconf');

const zipFileName = "s3zip.zip";

const credentials = new AWS.SharedIniFileCredentials({profile: 'CCNA_SharedServices_Architect'});
AWS.config.credentials = credentials;

const lambda = new AWS.Lambda({
  apiVersion : '2015-03-31',
  region : 'us-west-2'
});

nconf.argv();

co( function* () {
  try {
    fs.unlinkSync( zipFileName );
  } catch (err) {}
  yield spawn( '7zip', [
    '-r',
    zipFileName,
    'exports.js',
    'zipSchema.yaml',
    'node_modules'
  ]);

  let params = {
    FunctionName: 's3zip',
  };

  let result = null;
  
  if( nconf.get('create') ) {
    params.Code = {
      ZipFile: fs.readFileSync( zipFileName )
    };
    params.Handler = 'exports.handler';
    params.Role = 'arn:aws:iam::396279982172:role/CCNA_SharedServices_GCD_InstanceProfile';
    params.Runtime = 'nodejs4.3';
    params.Description = 'Zips files in an S3 bucket to the same or a different S3 Bucket';
    params.MemorySize = 256;
    params.Publish = true;
    params.Timeout = 300;

    result = yield lambda.createFunction(params).promise();
  } else {
    params.ZipFile = fs.readFileSync( zipFileName );
    result = yield lambda.updateFunctionCode(params).promise();
  }

  return result;
  
}).then( function(value) {
  console.log( value );
  process.exit(0);
}).catch( function(error) {
  console.log( error );
  process.exit(1);
});
